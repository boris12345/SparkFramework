import entity.User;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;
import spark.ModelAndView;
import spark.Request;
import spark.template.freemarker.FreeMarkerEngine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.sql.Connection.TRANSACTION_SERIALIZABLE;
import static spark.Spark.*;

public class Main {
    private static Sql2o db;
    private static FreeMarkerEngine engine;
    private static Configuration conf;

    public static void main(String[] args) {
        port(1234);
        staticFileLocation("src/main/resources");
        db = new Sql2o("jdbc:sqlite:src/main/java/db/play.txt", "", "");
        engine = new FreeMarkerEngine();
        conf = new Configuration();
        conf.setTemplateLoader(new ClassTemplateLoader(Main.class, "/"));
        engine.setConfiguration(conf);

        get("/", (req, res) -> initQueries());
        get("/hello", (req, res) -> "Hello World");
        get("/users", (req, res) -> {
            res.status(200);
            res.type("text/html");
            Map<String, User> attr = new HashMap<>();
            User u = new User();
            u.setUsername("boris");
            attr.put("username", u);
            return engine.render(new ModelAndView(attr, "result.ftl.html"));
        });
        get("/userView", (request, response) -> new String[]{"asf", "afs", "asfa"}[1]);
    }

    private static String initQueries() {
        Connection connection = db.beginTransaction(TRANSACTION_SERIALIZABLE);
        connection.createQuery("INSERT INTO users(username, password) VALUES('borko', 'borko')").executeUpdate();
        connection.createQuery("INSERT INTO users(username, password) VALUES('borko12', 'borko12')").executeUpdate();
        connection.createQuery("UPDATE users SET username = 'borkoTEST' WHERE username = 'borko12'").executeUpdate();
        connection.commit();

        return "Successful query";
    }

    private static User users(Request request) {
        Connection connection = db.beginTransaction(TRANSACTION_SERIALIZABLE);
        Query q = connection.createQuery("SELECT * FROM users");
        List<User> users = q.executeAndFetch(User.class);
        connection.commit();

        return users.get(0);
    }
}